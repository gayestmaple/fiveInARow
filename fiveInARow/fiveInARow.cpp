// fiveInARow.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "math.h"
#include "graphics.h"
#include "mmsystem.h"
#pragma comment (lib,"winmm.lib")

void GameMenu();
void InitGame();    //游戏加载函数
void DownChess();   //落子函数
void JudgeWin();    //输赢判断函数
void BackChess();   //悔棋函数

int flag = 1;       //黑白棋在数组棋盘上的数字显示  黑棋：1  白棋：-1
int a, b;           //数组棋盘对应位置的下标行列
int apre=0, bpre=0;     //记录上一次下载的位置
int chessboard[16][16];    //数组棋盘  用于存放黑白棋子
IMAGE nochess;
HWND hWnd;     //游戏窗口句柄  用于提示消息显示在当前窗口上层  防止被窗口遮挡

int main()
{
	while (1)
	{
		GameMenu();
		InitGame();       //游戏初始化
		DownChess();      //落棋子
		//system("pause");
	}
	return 0;
}

void GameMenu()
{
	//创建绘图窗口
	initgraph(600, 500);   // 创建绘图窗口，大小为 640x480 像素
	loadimage(NULL, _T(".//menu.jpg"));
	setbkcolor(WHITE);
	flag = 1;
	chessboard[16][16] = { 0 };
	MOUSEMSG mouse;        //用于保存接收鼠标消息
	while (1)
	{
		mouse = GetMouseMsg();
		if (mouse.x >= 196 && mouse.x <= 398 && mouse.y >= 341 && mouse.y <= 392)
		{
			if (mouse.uMsg == WM_LBUTTONDOWN)
			{
				break;
			}

		}
	}
}

void InitGame()
{
	hWnd = GetHWnd();                          //获取窗口句柄  提示窗口挂在次窗口下
	//加载图片
	loadimage(&nochess, _T(".//悔棋.jpg"));    //悔棋时用来贴在棋子处盖住棋子
	loadimage(NULL, _T(".//background.jpg"));  //加载背景图片
	//加载播放音乐
	mciSendString(_T("open music.mp3"), 0, 0, 0);
	mciSendString(_T("play music.mp3 repeat"), 0, 0, 0);
	//绘制棋盘
	setlinecolor(BLACK);        //设置线条颜色
	int i;                      //循环变量  用于绘制棋盘
	int x=25, y=25;             //棋盘左上角与窗口左上角的距离
	//绘制线条
	for (i = 0; i <= 15; i++)
	{
		line(x, 25, x, 475);
		x += 30;
		line(25, y, 475, y);
		y += 30;
	}
	//绘制消息区
	setlinecolor(GREEN);
	clearrectangle(490,50,590,450);
	rectangle(490, 50, 590, 450);
	rectangle(490-1, 50-1, 590-1, 450-1);
	rectangle(490-2, 50-2, 590-2, 450-2);
	settextcolor(BLACK);    //设置文本颜色
	setbkmode(0);           //文本背景透明度
	//输出文本
	outtextxy(500, 100, _T("玩家1:黑棋"));
	outtextxy(500, 120, _T("玩家2:白棋"));
	outtextxy(490, 150, _T(" 请玩家 "));
	clearrectangle(543, 150, 550, 165);
	outtextxy(543, 150, 1 + 48);
	outtextxy(550, 150, _T(" 落子"));
	//重新开始按钮
	setlinecolor(BLUE);
	setfillcolor(RGB(0,122,204));
	fillrectangle(500, 200, 580, 230);
	outtextxy(508,208,_T("重新开始"));
}

void DownChess()
{
	int i, j, x, y;
	MOUSEMSG mouse;        //用于保存接收鼠标消息
	while (1)
	{
		mouse = GetMouseMsg();   //获取鼠标消息
		//对鼠标位置的x，y处理，使不偏离线条交叉点
		for (i = 0; i < 16; i++)
		{
			for (j = 0; j < 16; j++)
			{
				if (abs(mouse.x - (i * 30+25)) < 15 && abs(mouse.y - (j * 30+25)) < 15)
				{
					a = i;
					b = j;
					x = i * 30 + 25;
					y = j * 30 + 25;
				}
			}
		}
		//对鼠标左键按下消息判断，按下则落子
		if (mouse.uMsg == WM_LBUTTONDOWN)
		{
			if (mouse.x < 580 && mouse.x>500 && mouse.y > 200 && mouse.y < 230)
			{
				return;
			}
			//先判断该位置是否有棋子
			if (chessboard[a][b] == 1 || chessboard[a][b] == -1)
			{
				BackChess();
				//MessageBox(hWnd, TEXT("此处已有棋子，请重新落子。"), TEXT("提示"), MB_OK);
				continue;
			}
			//如果没有，则落下相应颜色的棋子
			else
			{
				if (flag%2 != 0)  //黑棋
				{
					setfillcolor(BLACK);
					solidcircle(x, y, 10);
					//putimage(x-12.5,y-12.5,&blackchess);
					chessboard[a][b] = 1;
					apre = a; bpre = b;
					clearrectangle(543, 150, 550, 165);
					outtextxy(543, 150, 2 + 48);
					JudgeWin();
				}
				else if(flag%2 == 0)  //白棋
				{
					setfillcolor(WHITE);
					solidcircle(x, y, 10);
					//putimage(x - 12.5, y - 12.5, &whitechess);
					chessboard[a][b] = -1;
					apre = a, bpre = b;
					clearrectangle(543, 150, 550, 165);
					outtextxy(543, 150, 48 + 1);
					JudgeWin();
				}
				//flag = flag*(-1);
				flag++;
			}
		}
	}
}

void JudgeWin()
{
	int i = 0;
	int j = 0;
	//MOUSEMSG m;
	//m=GetMouseMsg();
	//if(m.x>=0&&m.x<=450+17&&m.y>=0&&m.y<=450+17)
	{
		for (i = 0; i < 15; i++)
		{
			for (j = 0; j < 15; j++)
			{
				if (chessboard[i][j] == 1 && chessboard[i + 1][j] == 1 && chessboard[i + 2][j] == 1 && chessboard[i + 3][j] == 1 && chessboard[i + 4][j] == 1)
				{
					MessageBox(hWnd, TEXT("黑子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (chessboard[i][j] == 1 && chessboard[i + 1][j + 1] == 1 && chessboard[i + 2][j + 2] == 1 && chessboard[i + 3][j + 3] == 1 && chessboard[i + 4][j + 4] == 1)
				{
					MessageBox(hWnd, TEXT("黑子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (chessboard[i][j + 1] == 1 && chessboard[i][j + 2] == 1 && chessboard[i][j + 3] == 1 && chessboard[i][j] == 1 && chessboard[i][j + 4] == 1)
				{
					MessageBox(hWnd, TEXT("黑子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}

				else if (i > 3 && chessboard[i][j] == 1 && chessboard[i + 1][j - 1] == 1 && chessboard[i + 2][j - 2] == 1 && chessboard[i + 3][j - 3] == 1 && chessboard[i + 4][j - 4] == 1)
				{
					MessageBox(hWnd, TEXT("黑子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (j > 3 && chessboard[i][j] == 1 && chessboard[i + 1][j - 1] == 1 && chessboard[i + 2][j - 2] == 1 && chessboard[i + 3][j - 3] == 1 && chessboard[i + 4][j - 4] == 1)
				{
					MessageBox(hWnd, TEXT("黑子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (chessboard[i][j] == -1 && chessboard[i + 1][j] == -1 && chessboard[i + 2][j] == -1 && chessboard[i + 3][j] == -1 && chessboard[i + 4][j] == -1)
				{
					MessageBox(hWnd, TEXT("白子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (chessboard[i][j] == -1 && chessboard[i + 1][j + 1] == -1 && chessboard[i + 2][j + 2] == -1 && chessboard[i + 3][j + 3] == -1 && chessboard[i + 4][j + 4] == -1)
				{
					MessageBox(hWnd, TEXT("白子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (chessboard[i][j + 1] == -1 && chessboard[i][j + 2] == -1 && chessboard[i][j + 3] == -1 && chessboard[i][j] == -1 && chessboard[i][j + 4] == -1)
				{
					MessageBox(hWnd, TEXT("白子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}

				else if (i > 3 && j > 3 && chessboard[i][j] == -1 && chessboard[i + 1][j - 1] == -1 && chessboard[i + 2][j - 2] == -1 && chessboard[i + 3][j - 3] == -1 && chessboard[i + 4][j - 4] == -1)
				{
					MessageBox(hWnd, TEXT("白子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
				else if (j > 3 && chessboard[i][j] == -1 && chessboard[i + 1][j - 1] == -1 && chessboard[i + 2][j - 2] == -1 && chessboard[i + 3][j - 3] == -1 && chessboard[i + 4][j - 4] == -1)
				{
					MessageBox(hWnd, TEXT("白子胜出，游戏结束"), TEXT("提示"), MB_OK);
				}
			}
		}
	}
}

void BackChess()
{
	int i, j, x, y;
	MOUSEMSG mouse;        //用于保存接收鼠标消息
	while (1)
	{
		mouse = GetMouseMsg();   //获取鼠标消息
		//对鼠标位置的x，y处理，使不偏离线条交叉点
		for (i = 0; i < 16; i++)
		{
			for (j = 0; j < 16; j++)
			{
				if (abs(mouse.x - (i * 30 + 25)) < 15 && abs(mouse.y - (j * 30 + 25)) < 15)
				{
					a = i;
					b = j;
					x = i * 30 + 25;
					y = j * 30 + 25;
				}
			}
		}
		//对鼠标左键按下消息判断，按下则落子
		if (mouse.uMsg == WM_RBUTTONDOWN)
		{
			//先判断该位置是否有棋子
			if (chessboard[a][b] == 1 && flag % 2 == 0 && a==apre && b == bpre)
			{
				MessageBox(hWnd, TEXT("黑棋悔棋"), TEXT("提示"), MB_OK);
				putimage(x - 14, y - 14, &nochess);
				//setfillcolor(NULL);
				//clearcircle(x, y, 10);
				chessboard[a][b] = 0;
				flag++;
				break;
			}
			//如果没有，则落下相应颜色的棋子
			else if (chessboard[a][b] == -1 && flag % 2 != 0 && a == apre && b == bpre)
			{
				MessageBox(hWnd, TEXT("白棋悔棋"), TEXT("提示"), MB_OK);
				//setfillcolor(NULL);
				putimage(x - 14, y - 14, &nochess);
				//clearcircle(x, y, 10);
				chessboard[a][b] = 0;
				flag++;
				break;
				//if (flag % 2 != 0)  //黑棋
				//{

				//}
				//else if (flag % 2 == 0)  //白棋
				//{

				//}
				//flag = flag*(-1);
				//flag++;
			}
			else
				break;
		}
	}
}


// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
